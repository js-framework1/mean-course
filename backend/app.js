const express = require('express');
const bodyParser = require('body-parser');

// Using post modal with mongoose schema
const mongoose = require('mongoose')
const Post = require('./models/posts')

const app = express();

// Connect to database
mongoose.connect("mongodb+srv://vinhnt:Xa34Avu6WOWygeBg@cluster0.7wz3f.mongodb.net/node-angular?retryWrites=true&w=majority")

  .then(() => {
    console.log('Connected to database!')
  })
  .catch((err) => {
    console.log('Connection false' + err);
  });

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: false}));
app.use((req, res, next) => {
  res.setHeader("Access-Control-Allow-Origin", "*");
  res.setHeader("Access-Control-Allow-Headers", "Origin, X-Request-With, Content-Type, Accept");
  res.setHeader("Access-Control-Allow-Methods", "GET, POST, PATCH, PUT, DELETE, OPTIONS");
  next();
})

/**
 * Add new post
 * */
app.post("/api/posts", (req, res, next) => {
  const post = new Post({
    title: req.body.title,
    content: req.body.content
  });
  post.save().then(createdPost => {
    res.status(201).json({
      message: 'Post added successfully',
      postId: createdPost._id
    });
  });

})

/**
 * Get list post
 * */
app.get("/api/posts", (req, res, next) => {
  Post.find()
    .then(documents => {
      res.status(200).json({
        message: 'Post fetched successfully',
        posts: documents
      });
    });
});

/**
 * Delete post by id
 */
app.delete("/api/posts/:id",(req, res, next) => {
 Post.deleteOne({_id: req.params.id})
   .then(result => {
     console.log(result);
   })
  res.status(200).json({
    message: 'Post deleted!'
  });
});

module.exports = app;
//Mongo user/pass: vinhnt/Xa34Avu6WOWygeBg
//host: mongodb+srv://vinhnt:<password>@cluster0.7wz3f.mongodb.net/<dbname>?retryWrites=true&w=majority
